// nnet/nnet-loss.cc

// Copyright 2011-2015  Brno University of Technology (author: Karel Vesely)

// See ../../COPYING for clarification regarding multiple authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
// WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
// MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache 2 License for the specific language governing permissions and
// limitations under the License.

#include "nnet/nnet-loss.h"
#include "nnet/nnet-utils.h"
#include "cudamatrix/cu-math.h"
#include "hmm/posterior.h"
#include <math.h>

#include <sstream>
#include <iterator>

namespace kaldi {
namespace nnet1 {


/* Xent */

/**
 * Helper function of Xent::Eval,
 * calculates number of matching elemente in 'v1', 'v2' weighted by 'weights'.
 */
template <typename T>
inline void CountCorrectFramesWeighted(const CuArray<T> &v1, 
                                       const CuArray<T> &v2, 
                                       const CuVectorBase<BaseFloat> &weights, 
                                       double *correct) {
  KALDI_ASSERT(v1.Dim() == v2.Dim());
  KALDI_ASSERT(v1.Dim() == weights.Dim());
  int32 dim = v1.Dim();
  // Get GPU data to host,
  std::vector<T> v1_h(dim), v2_h(dim);
  v1.CopyToVec(&v1_h);
  v2.CopyToVec(&v2_h);
  Vector<BaseFloat> w(dim);
  weights.CopyToVec(&w);
  // Get correct frame count (weighted),
  double corr = 0.0;
  for (int32 i=0; i<dim; i++) {
   corr += w(i) * (v1_h[i] == v2_h[i] ? 1.0 : 0.0);
  }
  // Return,
  (*correct) = corr;
}


void Xent::Eval(const VectorBase<BaseFloat> &frame_weights,
                const CuMatrixBase<BaseFloat> &net_out, 
                const CuMatrixBase<BaseFloat> &targets, 
                CuMatrix<BaseFloat> *diff) {
  // check inputs,
  KALDI_ASSERT(net_out.NumCols() == targets.NumCols());
  KALDI_ASSERT(net_out.NumRows() == targets.NumRows());
  KALDI_ASSERT(net_out.NumRows() == frame_weights.Dim());

  KALDI_ASSERT(KALDI_ISFINITE(frame_weights.Sum()));
  KALDI_ASSERT(KALDI_ISFINITE(net_out.Sum()));
  KALDI_ASSERT(KALDI_ISFINITE(targets.Sum()));

  // get frame_weights to GPU,
  frame_weights_ = frame_weights;

  // There may be frames for which the sum of targets is zero.
  // This happens in multi-lingual training when the frame 
  // has target class in the softmax of another language.
  // We 'switch-off' such frames by masking the 'frame_weights_',
  target_sum_.Resize(targets.NumRows());
  target_sum_.AddColSumMat(1.0, targets, 0.0);
  frame_weights_.MulElements(target_sum_);

  // get the number of frames after the masking,
  double num_frames = frame_weights_.Sum();
  KALDI_ASSERT(num_frames >= 0.0);

  // compute derivative wrt. activations of last layer of neurons,
  *diff = net_out;
  diff->AddMat(-1.0, targets);
  diff->MulRowsVec(frame_weights_); // weighting,

  // evaluate the frame-level classification,
  double correct; 
  net_out.FindRowMaxId(&max_id_out_); // find max in nn-output
  targets.FindRowMaxId(&max_id_tgt_); // find max in targets
  CountCorrectFramesWeighted(max_id_out_, max_id_tgt_, frame_weights_, &correct);

  // calculate cross_entropy (in GPU),
  xentropy_aux_ = net_out; // y
  xentropy_aux_.Add(1e-20); // avoid log(0)
  xentropy_aux_.ApplyLog(); // log(y)
  xentropy_aux_.MulElements(targets); // t*log(y)
  xentropy_aux_.MulRowsVec(frame_weights_); // w*t*log(y) 
  double cross_entropy = -xentropy_aux_.Sum();
  
  // caluculate entropy (in GPU),
  entropy_aux_ = targets; // t
  entropy_aux_.Add(1e-20); // avoid log(0)
  entropy_aux_.ApplyLog(); // log(t)
  entropy_aux_.MulElements(targets); // t*log(t)
  entropy_aux_.MulRowsVec(frame_weights_); // w*t*log(t) 
  double entropy = -entropy_aux_.Sum();

  KALDI_ASSERT(KALDI_ISFINITE(cross_entropy));
  KALDI_ASSERT(KALDI_ISFINITE(entropy));

  loss_ += cross_entropy;
  entropy_ += entropy;
  correct_ += correct;
  frames_ += num_frames;

  // progressive loss reporting
  {
    static const int32 progress_step = 3600*100; // 1h
    frames_progress_ += num_frames;
    loss_progress_ += cross_entropy;
    entropy_progress_ += entropy;
    if (frames_progress_ > progress_step) {
      KALDI_VLOG(1) << "ProgressLoss[last " 
                    << static_cast<int>(frames_progress_/100/3600) << "h of " 
                    << static_cast<int>(frames_/100/3600) << "h]: " 
                    << (loss_progress_-entropy_progress_)/frames_progress_ << " (Xent)";
      // store
      loss_vec_.push_back((loss_progress_-entropy_progress_)/frames_progress_);
      // reset
      frames_progress_ = 0;
      loss_progress_ = 0.0;
      entropy_progress_ = 0.0;
    }
  }
}


void Xent::Eval(const VectorBase<BaseFloat> &frame_weights,
                const CuMatrixBase<BaseFloat> &net_out, 
                const Posterior &post, 
                CuMatrix<BaseFloat> *diff) {
  int32 num_frames = net_out.NumRows(),
    num_pdf = net_out.NumCols();
  KALDI_ASSERT(num_frames == post.size());

  // convert posterior to matrix,
  PosteriorToMatrix(post, num_pdf, &tgt_mat_);

  // call the other eval function,
  Eval(frame_weights, net_out, tgt_mat_, diff);
}


std::string Xent::Report() {
  std::ostringstream oss;
  oss << "AvgLoss: " << (loss_-entropy_)/frames_ << " (Xent), "
      << "[AvgXent " << loss_/frames_ 
      << ", AvgTargetEnt " << entropy_/frames_ 
      << ", frames " << frames_ << "]" << std::endl;
  if (loss_vec_.size() > 0) {
     oss << "progress: [";
     std::copy(loss_vec_.begin(),loss_vec_.end(),std::ostream_iterator<float>(oss," "));
     oss << "]" << std::endl;
  }
  if (correct_ >= 0.0) {
    oss << "FRAME_ACCURACY >> " << 100.0*correct_/frames_ << "% <<" << std::endl;
  }
  return oss.str(); 
}


/* Mse */

void Mse::Eval(const VectorBase<BaseFloat> &frame_weights,
               const CuMatrixBase<BaseFloat>& net_out, 
               const CuMatrixBase<BaseFloat>& target, 
               CuMatrix<BaseFloat>* diff) {
  // check inputs,
  KALDI_ASSERT(net_out.NumCols() == target.NumCols());
  KALDI_ASSERT(net_out.NumRows() == target.NumRows());
  KALDI_ASSERT(net_out.NumRows() == frame_weights.Dim());

  KALDI_ASSERT(KALDI_ISFINITE(frame_weights.Sum()));
  KALDI_ASSERT(KALDI_ISFINITE(net_out.Sum()));
  KALDI_ASSERT(KALDI_ISFINITE(target.Sum()));

  int32 num_frames = frame_weights.Sum();
  KALDI_ASSERT(num_frames >= 0.0);

  // get frame_weights to GPU,
  frame_weights_ = frame_weights;

  //compute derivative w.r.t. neural nerwork outputs
  *diff = net_out; // y
  diff->AddMat(-1.0,target); // (y - t)
  diff->MulRowsVec(frame_weights_); // weighting,

  // Compute MeanSquareError loss of mini-batch
  diff_pow_2_ = *diff;
  diff_pow_2_.MulElements(diff_pow_2_); // (y - t)^2
  diff_pow_2_.MulRowsVec(frame_weights_); // w*(y - t)^2
  double mean_square_error = 0.5 * diff_pow_2_.Sum(); // sum the matrix,

  KALDI_ASSERT(KALDI_ISFINITE(mean_square_error));

  // accumulate
  loss_ += mean_square_error;
  frames_ += num_frames;

  // progressive loss reporting
  {
    static const int32 progress_step = 3600*100; // 1h
    frames_progress_ += num_frames;
    loss_progress_ += mean_square_error;
    if (frames_progress_ > progress_step) {
      KALDI_VLOG(1) << "ProgressLoss[last " 
                    << static_cast<int>(frames_progress_/100/3600) << "h of " 
                    << static_cast<int>(frames_/100/3600) << "h]: " 
                    << loss_progress_/frames_progress_ << " (Mse)";
      // store
      loss_vec_.push_back(loss_progress_/frames_progress_);
      // reset
      frames_progress_ = 0;
      loss_progress_ = 0.0;
    }
  }
}


void Mse::Eval(const VectorBase<BaseFloat> &frame_weights,
               const CuMatrixBase<BaseFloat>& net_out, 
               const Posterior& post, 
               CuMatrix<BaseFloat>* diff) {
  int32 num_frames = net_out.NumRows(),
    num_nn_outputs = net_out.NumCols();
  KALDI_ASSERT(num_frames == post.size());

  // convert posterior to matrix,
  PosteriorToMatrix(post, num_nn_outputs, &tgt_mat_);

  // call the other eval function,
  Eval(frame_weights, net_out, tgt_mat_, diff);
}
 

std::string Mse::Report() {
  // compute root mean square,
  int32 num_tgt = diff_pow_2_.NumCols();
  BaseFloat root_mean_square = sqrt(loss_/frames_/num_tgt);
  // build the message,
  std::ostringstream oss;
  oss << "AvgLoss: " << loss_/frames_ << " (Mse), " 
      << "[RMS " << root_mean_square << ", frames " << frames_ << "]" << std::endl;
  oss << "progress: [";
  std::copy(loss_vec_.begin(),loss_vec_.end(),std::ostream_iterator<float>(oss," "));
  oss << "]" << std::endl;
  return oss.str();
}


/* MultiTaskLoss */

void MultiTaskLoss::InitFromString(const std::string& s) {
  std::vector<std::string> v;
  SplitStringToVector(s, ",:" /* delimiter */, false, &v);

  KALDI_ASSERT((v.size()-1) % 3 == 0); // triplets,
  KALDI_ASSERT(v[0] == "multitask"); // header,

  // parse the definition of multitask loss,
  std::vector<std::string>::iterator it(v.begin()+1); // skip header,
  for ( ; it != v.end(); ++it) {
    // type,
    if (*it == "xent") {
      loss_vec_.push_back(new Xent());
    } else if (*it == "mse") {
      loss_vec_.push_back(new Mse());
    } else {
      KALDI_ERR << "Unknown objective function code : " << *it;
    }
    ++it;
    // dim,
    int32 dim;
    if (!ConvertStringToInteger(*it, &dim)) {
      KALDI_ERR << "Cannot convert 'dim' " << *it << " to integer!";
    }
    loss_dim_.push_back(dim);
    ++it;
    // weight,
    BaseFloat weight;
    if (!ConvertStringToReal(*it, &weight)) {
      KALDI_ERR << "Cannot convert 'weight' " << *it << " to integer!";
    }
    KALDI_ASSERT(weight >= 0.0);
    loss_weights_.push_back(weight);
  }

  // build vector with starting-point offsets,
  loss_dim_offset_.resize(loss_dim_.size()+1, 0); // 1st zero stays,
  for (int32 i = 1; i <= loss_dim_.size(); i++) {
    loss_dim_offset_[i] = loss_dim_offset_[i-1] + loss_dim_[i-1];
  }

  // sanity check,
  KALDI_ASSERT(loss_vec_.size() > 0);
  KALDI_ASSERT(loss_vec_.size() == loss_dim_.size());
  KALDI_ASSERT(loss_vec_.size() == loss_weights_.size());
}

void MultiTaskLoss::Eval(const VectorBase<BaseFloat> &frame_weights, 
            const CuMatrixBase<BaseFloat>& net_out, 
            const Posterior& post,
            CuMatrix<BaseFloat>* diff) {
  int32 num_frames = net_out.NumRows(),
    num_output = net_out.NumCols();
  KALDI_ASSERT(num_frames == post.size());
  KALDI_ASSERT(num_output == loss_dim_offset_.back()); // sum of loss-dims,

  // convert posterior to matrix,
  PosteriorToMatrix(post, num_output, &tgt_mat_);

  // allocate diff matrix,
  diff->Resize(num_frames, num_output);
  
  // call the vector of loss functions,
  CuMatrix<BaseFloat> diff_aux;
  for (int32 i = 0; i < loss_vec_.size(); i++) {
    loss_vec_[i]->Eval(frame_weights, 
      net_out.ColRange(loss_dim_offset_[i], loss_dim_[i]),
      tgt_mat_.ColRange(loss_dim_offset_[i], loss_dim_[i]),
      &diff_aux);
    // Scale the gradients,
    diff_aux.Scale(loss_weights_[i]);
    // Copy to diff,
    diff->ColRange(loss_dim_offset_[i], loss_dim_[i]).CopyFromMat(diff_aux);
  }
}

std::string MultiTaskLoss::Report() {
  // calculate overall loss (weighted),
  BaseFloat overall_loss = AvgLoss();
  // copy the loss-values into a vector,
  std::vector<BaseFloat> loss_values;
  for (int32 i = 0; i < loss_vec_.size(); i++) {
    loss_values.push_back(loss_vec_[i]->AvgLoss());
  }

  // build the message,
  std::ostringstream oss;
  oss << "MultiTaskLoss, with " << loss_vec_.size() << " parallel loss functions." << std::endl;
  // individual loss reports first,
  for (int32 i = 0; i < loss_vec_.size(); i++) {
    oss << "Loss " << i+1 << ", " << loss_vec_[i]->Report() << std::endl;
  }

  // overall loss is last,
  oss << "Loss (OVERALL), " 
      << "AvgLoss: " << overall_loss << " (MultiTaskLoss), "
      << "weights " << loss_weights_ << ", "
      << "values " << loss_values << std::endl;

  return oss.str();
}

BaseFloat MultiTaskLoss::AvgLoss() {
  BaseFloat ans(0.0);
  for (int32 i = 0; i < loss_vec_.size(); i++) {
    BaseFloat val = loss_weights_[i] * loss_vec_[i]->AvgLoss();
    if(!KALDI_ISFINITE(val)) {
      KALDI_WARN << "Loss " << i+1 << ", has bad objective function value '" << val << "', using 0.0 instead.";
      val = 0.0;
    }
    ans += val;
  }
  return ans;
}

/* MFoM for microF1 with Softmax layer network output */

void MifSoftmax::Eval(const VectorBase<BaseFloat> &frame_weights,
               const CuMatrixBase<BaseFloat> &net_out,
               const CuMatrixBase<BaseFloat> &target,
               CuMatrix<BaseFloat> *diff) {
  KALDI_ASSERT(net_out.NumRows() == target.NumRows());
  KALDI_ASSERT(net_out.NumCols() == target.NumCols());

  int32 num_rows = net_out.NumRows(), num_cols = net_out.NumCols();
  net_out_host_.Resize(num_rows, num_cols);
  net_out.CopyToMat(&net_out_host_);
  target_host_.Resize(num_rows, num_cols);
  target.CopyToMat(&target_host_);
  diff_host_.Resize(num_rows, num_cols);

  int32 tp = 0, fp = 0, np = 0;
  double smooth_tp = 0, smooth_fp = 0, l, dl[num_cols];

  for (int32 r = 0; r < num_rows; r++) {
    for (int32 c = 0; c < num_cols; c++) {
      l = 1.0 / (1.0 + (num_cols - 1) / (1.0 /  net_out_host_(r, c) - 1));
      if (target_host_(r, c) > 0) {
        np++;
        smooth_tp += 1.0 - l;
        if (l < 0.5) tp++;
      } else {
        smooth_fp += 1.0 - l;
        if (l < 0.5) fp++;
      }
      dl[c] = l * (1.0 - l);
    }
    for (int32 c = 0; c < num_cols; c++) {
      diff_host_(r, c) = dl[c] / (1.0 - net_out_host_(r, c));
      for (int32 xc = 0; xc < num_cols; xc++) {
        diff_host_(r, c) = diff_host_(r, c) - dl[xc] * net_out_host_(r, c) / (1.0 - net_out_host_(r, xc));
      }
    }
  }

  double scale_pos, scale_neg;
  scale_pos = -2.0 * (smooth_fp + np) / ((smooth_tp + smooth_fp + np) * (smooth_tp + smooth_fp + np));
  scale_neg = 2.0 * smooth_tp / ((smooth_tp + smooth_fp + np) * (smooth_tp + smooth_fp + np));
  for (int32 r = 0; r < num_rows; r++) {
    for (int32 c = 0; c < num_cols; c++) {
      if (target_host_(r, c) > 0) {
        diff_host_(r, c) = diff_host_(r, c) * scale_pos;
      } else {
        diff_host_(r, c) = diff_host_(r, c) * scale_neg;
      }
    }
  }
  diff->Resize(num_rows, num_cols);
  diff->CopyFromMat(diff_host_);
//std::cout << 200.0*smooth_tp/(smooth_tp+smooth_fp+np) << std::endl;
  frames_ += np;
  correct_ += tp;
  false_alarm_ += fp;
  // progressive loss reporting
  {
    static const int32 progress_step = 3600*100; // 1h
    frames_progress_ += np;
    correct_progress_ += tp;
    false_alarm_progress_ += fp;
    if (frames_progress_ > progress_step) {
      KALDI_VLOG(1) << "ProgressLoss[last "
                    << static_cast<int>(frames_progress_/100/3600) << "h of "
                    << static_cast<int>(frames_/100/3600) << "h]: "
                    << 100 - 200.0*correct_progress_/(correct_progress_+frames_progress_+false_alarm_progress_) << " (MifSoftmax)";
      // store
      loss_vec_.push_back(100 - 200.0*correct_progress_/(correct_progress_+frames_progress_+false_alarm_progress_));
      // reset
      frames_progress_ = 0;
      correct_progress_ = 0.0;
      false_alarm_progress_ = 0.0;
    }
  }
}


void MifSoftmax::Eval(const VectorBase<BaseFloat> &frame_weights,
               const CuMatrixBase<BaseFloat>& net_out,
               const Posterior& post,
               CuMatrix<BaseFloat>* diff) {
  int32 num_frames = net_out.NumRows(),
    num_nn_outputs = net_out.NumCols();
  KALDI_ASSERT(num_frames == post.size());

  // convert posterior to matrix,
  PosteriorToMatrix(post, num_nn_outputs, &tgt_mat_);

  // call the other eval function,
  Eval(frame_weights, net_out, tgt_mat_, diff);
}


std::string MifSoftmax::Report() {
  std::ostringstream oss;
  oss << "AvgLoss: " << 100.0 -  200.0*correct_/(correct_+frames_+false_alarm_);
  if (correct_ >= 0.0) {
    oss << "\nFRAME_ACCURACY >> " << 100.0*correct_/frames_ << "% <<";
  }
  oss << "progress: [";
  std::copy(loss_vec_.begin(),loss_vec_.end(),std::ostream_iterator<float>(oss," "));
  oss << "]" << std::endl;
  return oss.str();
}



/* MFoM for microF1 with Sigmoid network output */

void MifSigmoid::Eval(const VectorBase<BaseFloat> &frame_weights,
               const CuMatrixBase<BaseFloat> &net_out,
               const CuMatrixBase<BaseFloat> &target,
               CuMatrix<BaseFloat> *diff) {

  KALDI_ASSERT(net_out.NumRows() == target.NumRows());
  KALDI_ASSERT(net_out.NumCols() == target.NumCols());
  // number of active frames
  int32 num_frames = frame_weights.Sum();
  KALDI_ASSERT(num_frames >= 0.0);

  int32 num_rows = net_out.NumRows(), num_cols = net_out.NumCols();
  net_out_host_.Resize(num_rows, num_cols);
  net_out.CopyToMat(&net_out_host_);
  target_host_.Resize(num_rows, num_cols);
  target.CopyToMat(&target_host_);
  diff_host_.Resize(num_rows, num_cols);

  int32 np = 0; // TP + FN, i.e. sum of |C_k|
  int32 tp = 0; // true positive
  int32 fp = 0; // false rejection
  double smooth_tp = 0.;
  double smooth_fp = 0.;
  double l = 0.;
  
  // normalise sigmoid network output
  // according to MFoM inference
  // esigma = exp(s_k) / sum_j^M(exp[s_i])
  Matrix<BaseFloat> norm_out;
  norm_out.Resize(num_rows, num_cols);  

  for (int32 r = 0; r < num_rows; r++) {
    double sums = 0.0;
    for (int32 c = 0; c < num_cols; c++) {
        sums += exp(net_out_host_(r, c));
    }
    for (int32 c = 0; c < num_cols; c++) {
        norm_out(r, c) = exp(net_out_host_(r, c)) / sums;
    }
  }
  
  // calculate delta_l
  Matrix<BaseFloat> delta_l;
  delta_l.Resize(num_rows, num_cols);
  double ebeta = exp(-beta_param_);

  for (int32 r = 0; r < num_rows; r++) {
    for (int32 c = 0; c < num_cols; c++) {      
      double dk = 1. / (num_cols - 1) * (1.0 / norm_out(r, c) - 1.0);
      l = 1.0 / (1.0 + pow(dk, -alpha_param_) * ebeta);
      delta_l(r, c) = alpha_param_ * l * (1.0 - l);
      if (target_host_(r, c) > 0) {                  
        np++; // all positive labels: np = |C_k| = TP_k + FN_k
        smooth_tp += 1.0 - l;
        // class loss scores for discrete micro F1
        if (l < 0.5) tp++;
      } else {
        smooth_fp += 1.0 - l;
        if (l < 0.5) fp++;
      }
    }        
  }

  // scaling coefficients
  double a2 = pow((smooth_tp + smooth_fp + np), 2);
  // if x_i in C_k
  double scale_pos = 2.0 * (smooth_fp + np) / a2;
  // if x_i not in C_k
  double scale_neg = -2.0 * smooth_tp / a2;

  // sum of Jacobian matrices in every point x: [J_x^T * (w_1*y - w_2*\not_y)]
  for (int32 r = 0; r < num_rows; ++r) {
    for (int32 c = 0; c < num_cols; c++) {
      // diagonal elements
      double w = scale_pos * target_host_(r, c) - scale_neg * (1. - target_host_(r, c));
      double sum_dl = -delta_l(r, c) * w / (1.0 - norm_out(r, c));
      // off-diagonal
      for (int32 xc = 0; xc < num_cols; xc++) {
        w = scale_pos * target_host_(r, xc) - scale_neg * (1. - target_host_(r, xc));
        sum_dl += delta_l(r, c) * norm_out(r, c) * w / (1.0 - norm_out(r, xc));
      }
      ///
      double dsigma = net_out_host_(r, c) * (1.0 - net_out_host_(r, c));
      diff_host_(r, c) = dsigma * sum_dl;
    }
  }

  diff->Resize(num_rows, num_cols);
  diff->CopyFromMat(diff_host_);
  
  frames_ += num_frames;
  // micto F1 stats
  positive_samples_ += np;
  // for smoothed
  smooth_correct_ += smooth_tp;
  smooth_false_alarm_ += smooth_fp;
  // for discrete
  correct_ += tp;
  false_alarm_ += fp;

  // progressive smooth mF1 loss reporting
  {    
    static const int32 progress_step = 3600*100; // 1h
    frames_progress_ += num_frames;

    // micro F1 progress
    positive_samples_progress_ += np;
    smooth_correct_progress_ += smooth_tp;
    smooth_false_alarm_progress_ += smooth_fp;

    if (frames_progress_ > progress_step) {
      double smooth_error = 100.0 - 200.0 * smooth_correct_progress_ /
              (smooth_correct_progress_ + smooth_false_alarm_progress_ + positive_samples_progress_);
      //double discrete_error = 100.0 - 200.0 * tp / (tp + fp + np);
      KALDI_VLOG(1) << "ProgressLoss[last "
                    << static_cast<int>(frames_progress_/100/3600) << "h of "
                    << static_cast<int>(frames_/100/3600) << "h]: "
                    << smooth_error << " (Smooth MifSigmoid eror)";
      // store
      loss_vec_.push_back(smooth_error);
      // reset
      frames_progress_ = 0.0;
      positive_samples_progress_ = 0.0;
      smooth_correct_progress_ = 0.0;
      smooth_false_alarm_progress_ = 0.0;
    }
  }
}


void MifSigmoid::Eval(const VectorBase<BaseFloat> &frame_weights,
               const CuMatrixBase<BaseFloat>& net_out,
               const Posterior& post,
               CuMatrix<BaseFloat>* diff) {
  int32 num_frames = net_out.NumRows(),
    num_nn_outputs = net_out.NumCols();
  KALDI_ASSERT(num_frames == post.size());

  // convert posterior to matrix,
  PosteriorToMatrix(post, num_nn_outputs, &tgt_mat_);

  // call the other eval function,
  Eval(frame_weights, net_out, tgt_mat_, diff);
}


std::string MifSigmoid::Report() {
  std::ostringstream oss;  
  oss << "AvgLoss: " << 100.0 - 200.0*correct_/
         (correct_ + false_alarm_ + positive_samples_) << " (Mif-Sigmoid), ";
  oss << "Smooth AvgLoss: " << 100.0 - 200.0*smooth_correct_/
         (smooth_correct_ + smooth_false_alarm_ + positive_samples_) << " (Smooth-Mif-Sigmoid), ";
  oss << " Alpha param: " << alpha_param_;
  oss << " Beta param: " << beta_param_;
  oss << " True Positive: " << correct_;
  oss << " False Positive: " << false_alarm_;
  oss << " Positive Samples: " << positive_samples_;

  if (correct_ >= 0.0) {
    oss << "\nFRAME_ACCURACY >> " << 100.0*correct_/positive_samples_ << "% <<";
  }
  oss << "progress: [";
  std::copy(loss_vec_.begin(),loss_vec_.end(),std::ostream_iterator<float>(oss," "));
  oss << "]" << std::endl;
  return oss.str();
}


/* MFoM Units vs Zero learning strategy for microF1 with Sigmoid network output */

void MifUnitsVsZeros::Eval(const VectorBase<BaseFloat> &frame_weights,
               const CuMatrixBase<BaseFloat> &net_out,
               const CuMatrixBase<BaseFloat> &target,
               CuMatrix<BaseFloat> *diff) {

  KALDI_ASSERT(net_out.NumRows() == target.NumRows());
  KALDI_ASSERT(net_out.NumCols() == target.NumCols());
  // number of active frames
  int32 num_frames = frame_weights.Sum();
  KALDI_ASSERT(num_frames >= 0.0);

  int32 num_rows = net_out.NumRows(), num_cols = net_out.NumCols();
  net_out_host_.Resize(num_rows, num_cols);
  net_out.CopyToMat(&net_out_host_);
  target_host_.Resize(num_rows, num_cols);
  target.CopyToMat(&target_host_);
  diff_host_.Resize(num_rows, num_cols);

  int32 np = 0; // TP + FN, i.e. sum of |C_k|
  int32 tp = 0; // true positive
  int32 fp = 0; // false rejection
  double smooth_tp = 0.;
  double smooth_fp = 0.;
  double l = 0.;
  double dl[num_cols];

  // normalise sigmoid network output
  // according to MFoM inference
  // esigma = exp(s_k) / sum_j^M(exp[s_i])
  Matrix<BaseFloat> norm_out;
  norm_out.Resize(num_rows, num_cols);

  for (int32 r = 0; r < num_rows; r++) {
    double sums = 0.0;
    for (int32 c = 0; c < num_cols; c++) {
        sums += exp(net_out_host_(r, c));
    }
    for (int32 c = 0; c < num_cols; c++) {
        norm_out(r, c) = exp(net_out_host_(r, c)) / sums;
    }
  }

  // approximate microF1
  // double ebeta = exp(-beta_param_);

  for (int32 r = 0; r < num_rows; r++) {
    for (int32 c = 0; c < num_cols; c++) {
      // units vs zeros strategy
      double dk = 0.;
      if (is_evaluation_) {// during evaluation: sigmoid miclass measure          
          dk = -net_out_host_(r, c) + 0.5;
          l = 1.0 / (1.0 + exp(-alpha_param_ * dk - beta_param_));
          //dk = 1. / (num_cols - 1) * (1.0 / norm_out(r, c) - 1.0);
          //l = 1.0 / (1.0 + pow(dk, -alpha_param_) * ebeta);
          //l = 1 - net_out_host_(r, c); // i.e. sigmoids threshold
      } else { // during training: units vs zeros
        if (target_host_(r, c) > 0) {
          // antimodel is zeros
          double sumz = 0.;
          int n = 0;
          for (int32 i = 0; i < num_cols; i++){
              if (target_host_(r, i) < 1){
                  sumz += exp(net_out_host_(r, i));
                  n++;
              }
          }
          dk = -net_out_host_(r, c) + log(1./n * sumz);

        } else{
          // antimodel is units
          double sumu = 0.;
          int n = 0;
          for (int32 i = 0; i < num_cols; i++){
              if (target_host_(r, i) > 0){
                  sumu += exp(net_out_host_(r, i));
                  n++;
              }
          }
          dk = -net_out_host_(r, c) + log(1./n * sumu);
        }
        l = 1.0 / (1.0 + exp(-alpha_param_ * dk - beta_param_));
      }

      // smooth FP, TP using loss function
      if (target_host_(r, c) > 0) {
        // count all positive labels: np = |C_k| = TP_k + FN_k
        np++;
        smooth_tp += 1.0 - l;
        if (l < 0.5) tp++;
      } else {
        smooth_fp += 1.0 - l;
        if (l < 0.5) fp++;
      }
      dl[c] = alpha_param_ * l * (1.0 - l);
    }
    // calculate the derivative of dl: sum of Jacobian matrix elements by rows
    // d\sum_kl_k/dz_i = -dl_i + \sum_{k=1, k!=i} dl_k * s_i(z) / (1 - s_k(z))
    for (int32 c = 0; c < num_cols; c++) {
      double sum_dl = -dl[c] / (1.0 - norm_out(r, c));
      // WITHOUT off-diagonal Jacobian elements
      //for (int32 xc = 0; xc < num_cols; xc++) {
      //  sum_dl += dl[xc] * norm_out(r, c) / (1.0 - norm_out(r, xc));
      //}
      double dsigma = net_out_host_(r, c) * (1.0 - net_out_host_(r, c));
      diff_host_(r, c) = dsigma * sum_dl;
    }
  }

  double a2 = (smooth_tp + smooth_fp + np) * (smooth_tp + smooth_fp + np);
  // if x_i in C_k
  double scale_pos = 2.0 * (smooth_fp + np) / a2;
  // if x_i not in C_k
  double scale_neg = -2.0 * smooth_tp / a2;

  for (int32 r = 0; r < num_rows; r++) {
    for (int32 c = 0; c < num_cols; c++) {
      if (target_host_(r, c) > 0) {
        diff_host_(r, c) = diff_host_(r, c) * scale_pos;
      } else {
        diff_host_(r, c) = diff_host_(r, c) * scale_neg;
      }
    }
  }
  diff->Resize(num_rows, num_cols);
  diff->CopyFromMat(diff_host_);

  frames_ += num_frames;
  // micto F1 stats
  positive_samples_ += np;
  // for smoothed
  smooth_correct_ += smooth_tp;
  smooth_false_alarm_ += smooth_fp;
  // for discrete
  correct_ += tp;
  false_alarm_ += fp;

  // progressive smooth mF1 loss reporting
  {
    static const int32 progress_step = 3600*100; // 1h
    frames_progress_ += num_frames;

    // micro F1 progress
    positive_samples_progress_ += np;
    smooth_correct_progress_ += smooth_tp;
    smooth_false_alarm_progress_ += smooth_fp;

    if (frames_progress_ > progress_step) {
      double smooth_error = 100.0 - 200.0 * smooth_correct_progress_ /
              (smooth_correct_progress_ + smooth_false_alarm_progress_ + positive_samples_progress_);
      //double discrete_error = 100.0 - 200.0 * tp / (tp + fp + np);
      KALDI_VLOG(1) << "ProgressLoss[last "
                    << static_cast<int>(frames_progress_/100/3600) << "h of "
                    << static_cast<int>(frames_/100/3600) << "h]: "
                    << smooth_error << " (Smooth MifUnitsVsZeros eror)";
      // store
      loss_vec_.push_back(smooth_error);
      // reset
      frames_progress_ = 0.0;
      positive_samples_progress_ = 0.0;
      smooth_correct_progress_ = 0.0;
      smooth_false_alarm_progress_ = 0.0;
    }
  }
}

void MifUnitsVsZeros::Eval(const VectorBase<BaseFloat> &frame_weights,
               const CuMatrixBase<BaseFloat>& net_out,
               const Posterior& post,
               CuMatrix<BaseFloat>* diff) {
  int32 num_frames = net_out.NumRows(),
    num_nn_outputs = net_out.NumCols();
  KALDI_ASSERT(num_frames == post.size());

  // convert posterior to matrix,
  PosteriorToMatrix(post, num_nn_outputs, &tgt_mat_);

  // call the other eval function,
  Eval(frame_weights, net_out, tgt_mat_, diff);
}


std::string MifUnitsVsZeros::Report() {
  std::ostringstream oss;
  oss << "AvgLoss: " << 100.0 - 200.0*correct_/
         (correct_ + false_alarm_ + positive_samples_) << " (MifUnitsVsZeros), ";
  oss << "Smooth AvgLoss: " << 100.0 - 200.0*smooth_correct_/
         (smooth_correct_ + smooth_false_alarm_ + positive_samples_) << " (Smooth-MifUnitsVsZeros), ";

  oss << " Alpha param: " << alpha_param_;
  oss << " Beta param: " << beta_param_;
  oss << " True Positive: " << correct_;
  oss << " False Positive: " << false_alarm_;
  oss << " Positive Samples: " << positive_samples_;

  if (correct_ >= 0.0) {
    oss << "\nFRAME_ACCURACY >> " << 100.0*correct_/positive_samples_ << "% <<";
  }
  oss << "progress: [";
  std::copy(loss_vec_.begin(),loss_vec_.end(),std::ostream_iterator<float>(oss," "));
  oss << "]" << std::endl;
  return oss.str();
}


/* Mse with Sigmoid output units */

void MseSigmoid::Eval(const VectorBase<BaseFloat> &frame_weights,
               const CuMatrixBase<BaseFloat>& net_out,
               const CuMatrixBase<BaseFloat>& target,
               CuMatrix<BaseFloat>* diff) {
  // check inputs,
  KALDI_ASSERT(net_out.NumCols() == target.NumCols());
  KALDI_ASSERT(net_out.NumRows() == target.NumRows());
  KALDI_ASSERT(net_out.NumRows() == frame_weights.Dim());

  KALDI_ASSERT(KALDI_ISFINITE(frame_weights.Sum()));
  KALDI_ASSERT(KALDI_ISFINITE(net_out.Sum()));
  KALDI_ASSERT(KALDI_ISFINITE(target.Sum()));

  int32 num_frames = frame_weights.Sum();
  KALDI_ASSERT(num_frames >= 0.0);

  // get frame_weights to GPU,
  frame_weights_ = frame_weights;

  //compute derivative w.r.t. neural nerwork outputs
  *diff = net_out; // y
  diff->AddMat(-1.0,target); // (y - t)
  diff_pow_2_ = *diff;

  /// fixing for the case of MSE loss and sigmoid output layer
  /// (y - t) * y * (1 - y) = (y - t) * y - (y - t) * y * y
  diff->MulElements(net_out); // (y - t) * y
  CuMatrix<BaseFloat> tmp = *diff;
  tmp.MulElements(net_out); // (y - t) * y * y
  diff->AddMat(-1.0,tmp); // (y - t) * y * (1 - y)
  ///
  diff->MulRowsVec(frame_weights_); // weighting,

  // Compute MeanSquareError loss of mini-batch
  diff_pow_2_.MulElements(diff_pow_2_); // (y - t)^2
  diff_pow_2_.MulRowsVec(frame_weights_); // w*(y - t)^2
  double mean_square_error = 0.5 * diff_pow_2_.Sum(); // sum the matrix,

  KALDI_ASSERT(KALDI_ISFINITE(mean_square_error));

  // accumulate
  loss_ += mean_square_error;
  frames_ += num_frames;

  // progressive loss reporting
  {
    static const int32 progress_step = 3600*100; // 1h
    frames_progress_ += num_frames;
    loss_progress_ += mean_square_error;
    if (frames_progress_ > progress_step) {
      KALDI_VLOG(1) << "ProgressLoss[last "
                    << static_cast<int>(frames_progress_/100/3600) << "h of "
                    << static_cast<int>(frames_/100/3600) << "h]: "
                    << loss_progress_/frames_progress_ << " (Mse)";
      // store
      loss_vec_.push_back(loss_progress_/frames_progress_);
      // reset
      frames_progress_ = 0;
      loss_progress_ = 0.0;
    }
  }
}

void MseSigmoid::Eval(const VectorBase<BaseFloat> &frame_weights,
               const CuMatrixBase<BaseFloat>& net_out,
               const Posterior& post,
               CuMatrix<BaseFloat>* diff) {
  int32 num_frames = net_out.NumRows(),
    num_nn_outputs = net_out.NumCols();
  KALDI_ASSERT(num_frames == post.size());

  // convert posterior to matrix,
  PosteriorToMatrix(post, num_nn_outputs, &tgt_mat_);

  // call the other eval function,
  Eval(frame_weights, net_out, tgt_mat_, diff);
}

std::string MseSigmoid::Report() {
  // compute root mean square,
  int32 num_tgt = diff_pow_2_.NumCols();
  BaseFloat root_mean_square = sqrt(loss_/frames_/num_tgt);
  // build the message,
  std::ostringstream oss;
  oss << "AvgLoss: " << loss_/frames_ << " (MseSigmoid), "
      << "[RMS " << root_mean_square << ", frames " << frames_ << "]" << std::endl;
  oss << "progress: [";
  std::copy(loss_vec_.begin(),loss_vec_.end(),std::ostream_iterator<float>(oss," "));
  oss << "]" << std::endl;
  return oss.str();
}

} // namespace nnet1
} // namespace kaldi
